<?php

/**
 * @Author: tekin
 * @Date:   2017-10-06 15:07:18
 * @Last Modified 2017-10-06
 */

require_once(dirname(__FILE__).'/Pinyin.php');

//$pinyin = new Pinyin();
$py = new Pinyin();
//echo $py->result('拼音转换', false, true);



//加载PHPQR类
require_once dirname(__FILE__).'/phpqrcode.php';

/**
 * 二维码生成函数 可带LOGO
 * @param  [type]  $value                [二维码内容]
 * @param  integer $logo                  [二维码LOGO路径]
 * @param  string  $errorCorrectionLevel [容错级别]
 * @param  integer $matrixPointSize      [二维码像素大小]
 * @return [type]                        [二维码图片]
 */
function make_qrcode($value, $logo = 0, $errorCorrectionLevel = 'L', $matrixPointSize = 5) {

    //生成二维码图片
    $QR = dirname(__FILE__).'/qrcode_'.substr(md5($logo.$value),0,5).'.png';
    QRcode::png($value, $QR, $errorCorrectionLevel, $matrixPointSize, 3);
    $QR = imagecreatefromstring(file_get_contents($QR));
    if ($logo) {
        $logo = imagecreatefromstring(file_get_contents($logo));
        $QR_width = imagesx($QR);//二维码图片宽度
        $QR_height = imagesy($QR);//二维码图片高度
        $logo_width = imagesx($logo);//logo图片宽度
        $logo_height = imagesy($logo);//logo图片高度
        $logo_qr_width = $QR_width / 4;
        $scale = $logo_width/$logo_qr_width;
        $logo_qr_height = $logo_height/$scale;
        $from_width = ($QR_width - $logo_qr_width) / 2;
        //重新组合图片并调整大小
        imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
    }
     // 输出图片
	ob_start();
	ob_clean();
	header("Content-type: image/png");
	$png = ImagePng($QR);
   return  $png;
}
 
echo make_qrcode("http://cms.yunnan.ws/");


