<?php

/**
 * @Author: Tekin
 * @Date:   2018-05-15 19:48:26
 * @Last Modified 2018-05-15
 */

if (!function_exists('pp')) {
    /**
     * 格式化输出
     * @param  [type] $arr [description]
     * @return [type]      [description]
     */
    function pp($arr)
    {
        if (is_array($arr)) {
            echo "<pre>";
            print_r($arr);
            echo "</pre>";
        } else if (is_object($arr)) {
            echo "<pre>";
            print_r($arr);
            echo "</pre>";

        } else {
            echo $arr;
        }
        die;
    }
}

pp($_POST);
