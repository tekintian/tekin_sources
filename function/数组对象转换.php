<?php

/**
 * @Author: tekin
 * @Date:   2017-09-17 11:15:04
 * @Last Modified 2017-09-17
 */

//如果不是二维数组返回true
function is_two_array($array){
	  return count($array)==count($array, 1);
}

//对象表单选项转换
function set_obj_form_item($_data, $_key, $_value) {
	$_items = array();
	if (is_array($_data)) {
		foreach ($_data as $_v) {
			$_items[$_v->$_key] = $_v->$_value;
		}
	}
	return $_items;
}
//数组表单转换
function set_array_form_item($_data, $_key, $_value) {
		$_items = array();
		if (is_array($_data)) {
			foreach ($_data as $_v) {
				$_items[$_v[$_key]] = $_v[$_value];
			}
		}
	return $_items;
}

//把对象数组转换为关联数组的方法
function get_object_vars_final($obj){
	if(is_object($obj)){
		$obj=get_object_vars($obj);
	}
	if(is_array($obj)){
		foreach ($obj as $key=>$value){
			$obj[$key]=get_object_vars_final($value);
		}
	}
	return $obj;
}
