<?php

/**
 * @Author: Tekin
 * @Date:   2018-10-29 11:21:48
 * @Last Modified 2018-10-29* @Last Modified time: 2018-10-29 11:21:48
 */

/**
 * 自动捕获Fatal Error
 */
register_shutdown_function( "fatal_handler" );/*注册一个callback方法*/
set_error_handler("error_handler");/* 设置用户自定义的错误处理函数*/
define('E_FATAL',  E_ERROR | E_USER_ERROR |  E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR| E_PARSE );


//获取fatal error
function fatal_handler()
{
    $error = error_get_last();
    if($error && ($error["type"]===($error["type"] & E_FATAL)))
    {
        error_handler($error["type"],$error["message"],$error["file"],$error["line"]);
    }
}
/*获取所有的error*/
function error_handler($errno,$errstr,$errfile,$errline)
{
    $str=<<<EOF
     "errno":$errno
     "errstr":$errstr
     "errfile":$errfile
     "errline":$errline
EOF;
    /*获取到错误可以自己处理，比如记Log、报警等等*/
    echo $str;