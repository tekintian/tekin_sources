<?php

/**
 * @Author: tekin
 * @Date:   2017-09-17 11:29:35
 * @Last Modified 2018-10-29
 */




//转换剩余时间格式
function gettime($time){
    if ($time < 0) {  
        return '已结束';  
    } else {  
        if ($time < 60) {  
            return $time . '秒';  
        } else {  
            if ($time < 3600) {  
                return floor($time / 60) . '分钟';  
            } else {  
                if ($time < 86400) {  
                    return floor($time / 3600) . '小时';  
                } else {  
                    if ($time < 259200) {//3天内  
                        return floor($time / 86400) . '天';  
                    } else {  
                        return floor($time / 86400) . '天';  
                    }  
                }  
            }  
        }  
    }  
}

/**
 * 显示距离当前时间的字符串
 * @author TekinTian <tekintian@gmail.com>
 * @params  $time time()|string 
 */
function tran_time($time)
{
    if(!is_numeric($time))
    {
        if(!strtotime($time)) throw new Exception('The parameters are not in format');
        $time = strtotime($time);
    }

    $rtime = date("m-d H:i",$time);
    $htime = date("H:i",$time);

    $time = time() - $time;
    if ($time < 60)
    {
        $str = '刚刚';
    }
    elseif ($time < 60 * 60)
    {
        $min = floor($time/60);
        $str = $min.'分钟前';
    }
    elseif ($time < 60 * 60 * 24)
    {
        $h = floor($time/(60*60));
        $str = $h.'小时前 '.$htime;
    }
    elseif ($time < 60 * 60 * 24 * 3)
    {
        $d = floor($time/(60*60*24));
        if($d==1)
            $str = '昨天 '.$rtime;
        else
            $str = '前天 '.$rtime;
    }
    else
    {
        $str = $rtime;
    }
    return $str;
}