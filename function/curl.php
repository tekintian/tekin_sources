<?php

/**
 * @Author: tekin
 * @Date:   2017-09-17 11:13:18
 * @Last Modified 2018-10-29
 */
//curlRequest 请求函数
function curl_request($url,$data,$method='POST'){
    $ch = curl_init();//初始化CURL句柄 
    curl_setopt($ch, CURLOPT_URL, $url); //设置请求的URL
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //设为TRUE把curl_exec()结果转化为字串，而不是直接输出 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //设置请求方式
    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("X-HTTP-Method-Override: $method")); //设置HTTP头信息
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); //设置提交的字符串
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));
    $list = curl_exec($ch); //执行预定义的CURL 
    if(!curl_errno($ch)){
        $result = array('status'=>1,'info'=>$list);
        //$info = curl_getinfo($ch); 
        //'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
    } else { 
        //echo 'Curl error: ' . curl_error($ch);
        $result = array('status'=>0,'info'=>curl_error($ch));
    }
    curl_close($ch);
    return $result;
}



/**
 * 远程数据获取发送
 * @param  [type] $url        [description]
 * @param  [type] $postFields [description]
 * @return [type]             [description]
 */
function curl($url, $postFields = null)
{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FAILONERROR, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if (is_array($postFields) && 0 < count($postFields))
		{
			$postBodyString = "";
			foreach ($postFields as $k => $v)
			{
				$postBodyString .= "$k=" . urlencode($v) . "&"; 
			}
			unset($k, $v);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);  
 			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, substr($postBodyString,0,-1));
		}
		$reponse = curl_exec($ch);
		curl_close($ch);
		return $reponse;
}


/**
 * Curl 请求
 * @param  [type]  $url    [请求的URL地址]
 * @param  [type]  $data   [发送的数据]
 * @param  integer $isjson [是否JSON格式]
 * @return [type]          [返回信息]
 */
function http_curl_request($url, $data = null, $isjson = 0)
{
    $curl = curl_init(); // 启动一个CURL会话
    curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    if ($data != null) {
        if ($isjson == 0) {
            $data = http_build_query($data);
        }
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    }
    curl_setopt($curl, CURLOPT_TIMEOUT, 300); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $data = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno:' . curl_getinfo($curl); //捕抓异常
        var_dump(curl_getinfo($curl));
    }

    // Parse the returned XML data to an array
    preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $match);
    $result = array();
    foreach ($match[1] as $x => $y)
    {
        $result[$y] = $match[2][$x];
    }
    return $result;
}


//for demo
$xml=<<<EOF
<status>success</status>
<statusmsg>online</statusmsg>
<vmstat>online</vmstat>
<hostname>s4.tekin.cn</hostname>
<ipaddress>144.172.84.93</ipaddress>
EOF;



