<?php


// 时间查看
function timeTran($the_time) {
    $now_time = date("Y-m-d H:i:s", time());
    $now_time = strtotime($now_time);
    $show_time = strtotime($the_time);
    $dur = $now_time - $show_time;
    if ($dur < 0) {
        return $the_time;
    } else {
        if ($dur < 60) {
            return $dur . '秒前';
        } else {
            if ($dur < 3600) {
                return floor($dur / 60) . '分钟前';
            } else {
                if ($dur < 86400) {
                    return floor($dur / 3600) . '小时前';
                } else {
                    if ($dur < 259200) { //3天内
                        return floor($dur / 86400) . '天前';
                    } else {
                        return $the_time;
                    }
                }
            }
        }
    }
}
// 验证码
function check_verify($code, $id = ''){
    $verify = new \Think\Verify();
    return $verify->check($code, $id);
}

//图片裁剪
function cut_image($img, $width, $height, $type = 3){
    if(empty($width)&&empty($height)){
        return $img;
    }
    $imgDir = realpath(ROOT_PATH.$img);
    if(!is_file($imgDir)){
        return $img;
    }
    $imgInfo = pathinfo($img);
    $newImg = $imgInfo['dirname'].'/cut_'.$width.'_'.$height.'_'.$imgInfo["basename"];
    $newImgDir = ROOT_PATH.$newImg;
    if(!is_file($newImgDir)){
        $image = new \Think\Image();
        $image->open($imgDir);
        $image->thumb($width, $height,$type)->save($newImgDir);
    }
    return $newImg;
}

//默认数据
function default_data($data,$var){
	$info = !empty($data) ? $data: $var;
    return $info;
}

//获取用户相关信息
function user($uin,$field){
    if ($field) {
        $info = M('user')->where(array('uin'=>$uin))->getField($field);
    }else{
        $info = M('user')->where(array('uin'=>$uin))->find();
    }
    return  $info;
}

//新闻分类调用函数 获取新闻分类相关信息
function news_category($id,$field){
	$info = M('news_category')->where(array('id'=>$id))->find();
	$info['url'] = U('news/index',array('id'=>$id));
	return  $field ? $info[$field]: $info;
}

//项目分类调用函数 获取项目分类相关信息
function item_category($id,$field){
    $info = M('item_category')->where(array('id'=>$id))->find();
    $info['url'] = U('Item/index',array('id'=>$id));
    return  $field ? $info[$field]: $info;
}

/**
 * 加密密码
 * @param string    $data   待加密字符串
 * @return string 返回加密后的字符串
 */

function encrypt($data) {
    return md5(md5(C('AUTH_CODE').md5($data)));
    //return md5(C("AUTH_CODE") . md5($data));
}

//增加用户操作记录
function user_log($uin,$con,$type){
	$data['uin']=$uin;
	$data['con']=$con;
	$data['type']=$type;
	$data['time']=time();
	if(M('user_do_log')->token(true)->add($data)){
		return true;
	}else{
		return false;
	}
}

//转换剩余时间格式
function gettime($time){
    if ($time < 0) {  
        return '已结束';  
    } else {  
        if ($time < 60) {  
            return $time . '秒';  
        } else {  
            if ($time < 3600) {  
                return floor($time / 60) . '分钟';  
            } else {  
                if ($time < 86400) {  
                    return floor($time / 3600) . '小时';  
                } else {  
                    if ($time < 259200) {//3天内  
                        return floor($time / 86400) . '天';  
                    } else {  
                        return floor($time / 86400) . '天';  
                    }  
                }  
            }  
        }  
    }  
}

/**
 * 将一个字符串部分字符用*替代隐藏
 * @param string    $string   待转换的字符串
 * @param int       $bengin   起始位置，从0开始计数，当$type=4时，表示左侧保留长度
 * @param int       $len      需要转换成*的字符个数，当$type=4时，表示右侧保留长度
 * @param int       $type     转换类型：0，从左向右隐藏；1，从右向左隐藏；2，从指定字符位置分割前由右向左隐藏；3，从指定字符位置分割后由左向右隐藏；4，保留首末指定字符串
 * @param string    $glue     分割符
 * @return string   处理后的字符串
 */
function hideStr($string, $bengin = 0, $len = 4, $type = 0, $glue = "@") {
    if (empty($string))
        return false;
    $array = array();
    if ($type == 0 || $type == 1 || $type == 4) {
        $strlen = $length = mb_strlen($string);
        while ($strlen) {
            $array[] = mb_substr($string, 0, 1, "utf8");
            $string = mb_substr($string, 1, $strlen, "utf8");
            $strlen = mb_strlen($string);
        }
    }
    switch ($type) {
        case 1:
            $array = array_reverse($array);
            for ($i = $bengin; $i < ($bengin + $len); $i++) {
                if (isset($array[$i]))
                    $array[$i] = "*";
            }
            $string = implode("", array_reverse($array));
            break;
        case 2:
            $array = explode($glue, $string);
            $array[0] = hideStr($array[0], $bengin, $len, 1);
            $string = implode($glue, $array);
            break;
        case 3:
            $array = explode($glue, $string);
            $array[1] = hideStr($array[1], $bengin, $len, 0);
            $string = implode($glue, $array);
            break;
        case 4:
            $left = $bengin;
            $right = $len;
            $tem = array();
            for ($i = 0; $i < ($length - $right); $i++) {
                if (isset($array[$i]))
                    $tem[] = $i >= $left ? "*" : $array[$i];
            }
            $array = array_chunk(array_reverse($array), $right);
            $array = array_reverse($array[0]);
            for ($i = 0; $i < $right; $i++) {
                $tem[] = $array[$i];
            }
            $string = implode("", $tem);
            break;
        default:
            for ($i = $bengin; $i < ($bengin + $len); $i++) {
                if (isset($array[$i]))
                    $array[$i] = "*";
            }
            $string = implode("", $array);
            break;
    }
    return $string;
}

/**
 * 功能：字符串截取指定长度
 * @param string    $string      待截取的字符串
 * @param int       $len         截取的长度
 * @param int       $start       从第几个字符开始截取
 * @param boolean   $suffix      是否在截取后的字符串后跟上省略号
 * @return string               返回截取后的字符串
 */
function cutStr($str, $len = 100, $start = 0, $suffix = 1) {
    $str = strip_tags(trim(strip_tags($str)));
    $str = str_replace(array("\n", "\t"), "", $str);
    $strlen = mb_strlen($str);
    while ($strlen) {
        $array[] = mb_substr($str, 0, 1, "utf8");
        $str = mb_substr($str, 1, $strlen, "utf8");
        $strlen = mb_strlen($str);
    }
    $end = $len + $start;
    $str = '';
    for ($i = $start; $i < $end; $i++) {
        $str.=$array[$i];
    }
    return count($array) > $len ? ($suffix == 1 ? $str . "&hellip;" : $str) : $str;
}

//curlRequest 请求函数
function curlRequest($url,$data,$method='POST'){
    $ch = curl_init();//初始化CURL句柄 
    curl_setopt($ch, CURLOPT_URL, $url); //设置请求的URL
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); //设为TRUE把curl_exec()结果转化为字串，而不是直接输出 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //设置请求方式
    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("X-HTTP-Method-Override: $method")); //设置HTTP头信息
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data)); //设置提交的字符串
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));
    $list = curl_exec($ch); //执行预定义的CURL 
    if(!curl_errno($ch)){
        $result = array('status'=>1,'info'=>$list);
        //$info = curl_getinfo($ch); 
        //'Took ' . $info['total_time'] . ' seconds to send a request to ' . $info['url'];
    } else { 
        //echo 'Curl error: ' . curl_error($ch);
        $result = array('status'=>0,'info'=>curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

//读取栏目模型
function modelName($mid){
    if ($mid) {
        $info = M('model')->field('model_name')->where(array('id'=>$mid))->find();
    }else{
        return '文章模型';
    }
    return  $info['model_name'];
}

/**
 * 获取文件目录列表
 * @param string $pathname 路径
 * @param integer $fileFlag 文件列表 0所有文件列表,1只读文件夹,2是只读文件(不包含文件夹)
 * @param string $pattern 文件名称
 * @return array
 */
function ListModel($pathname,$fileFlag = 0, $pattern='*') {
    $fileArray = array();
    $pathname = rtrim($pathname,'/') . '/';
    $list   =   glob($pathname.$pattern);
    foreach ($list  as $i => $file) {
        switch ($fileFlag) {
            case 0:
                $fileArray[]=basename($file);
                break;
            case 1:
                if (is_dir($file)) {
                    $fileArray[]=basename($file);
                }
                break;

            case 2:
                if (is_file($file)) {
                    $fileArray[]=basename($file);
                }
                break;

            default:
                break;
        }
    }

    if(empty($fileArray)) $fileArray = NULL;
    return $fileArray;
}
/**
 * 获取IP地址
 *
 */
function GetIP(){
    $ip = false;
    if(!empty($_SERVER["HTTP_CLIENT_IP"])){
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ips = explode (", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip) { array_unshift($ips, $ip); $ip = FALSE; }
        for ($i = 0; $i < count($ips); $i++) {
            if (!eregi ("^(10│172.16│192.168).", $ips[$i])) {
                $ip = $ips[$i];
                break;
            }
        }
    }
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}


/**
 * 检查是否是以手机浏览器进入(IN_MOBILE)
 */
function isMobile() {
	$mobile = array ();
	static $mobilebrowser_list = 'Mobile|iPhone|Android|WAP|NetFront|JAVA|OperasMini|UCWEB|WindowssCE|Symbian|Series|webOS|SonyEricsson|Sony|BlackBerry|Cellphone|dopod|Nokia|samsung|PalmSource|Xphone|Xda|Smartphone|PIEPlus|MEIZU|MIDP|CLDC';
	// note 获取手机浏览器
	if (preg_match ( "/$mobilebrowser_list/i", $_SERVER ['HTTP_USER_AGENT'], $mobile )) {
		return true;
	} else {
		if (preg_match ( '/(mozilla|chrome|safari|opera|m3gate|winwap|openwave)/i', $_SERVER ['HTTP_USER_AGENT'] )) {
			return false;
		} else {
			if ($_GET ['mobile'] === 'yes') {
				return true;
			} else {
				return false;
			}
		}
	}
}
function isiPhone() {
	return strpos ( $_SERVER ['HTTP_USER_AGENT'], 'iPhone' ) !== false;
}
function isiPad() {
	return strpos ( $_SERVER ['HTTP_USER_AGENT'], 'iPad' ) !== false;
}
function isiOS() {
	return isiPhone () || isiPad ();
}
function isAndroid() {
	return strpos ( $_SERVER ['HTTP_USER_AGENT'], 'Android' ) !== false;
}


 //读取站点信息
function hostName($mid){
    if ($mid) {
        $info = M('host','oc_')->where(array('id'=>$mid))->find();
    }else{
        return '不知来源';
    }
    return  $info['name'];
}
// 签名校验
function checkSign($sign_key, $data){
    ksort($data);
    $new_sign_key = sha1(http_build_query($data));
    return $sign_key == $new_sign_key;
}

/**
 * 过滤数据
 * @param  array  $data 过滤数据
 */
function filter_string($data){
    if($data===NULL){
        return false;
    }
    if (is_array($data)){
        foreach ($data as $k=>$v){
            $data[$k] = filter_string($v);
        }
        return $data;
    }else{
        return htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
    }
}

/**
 * 数据签名认证
 * @param  array  $data 被认证的数据
 * @return string       签名
 */
function data_auth_sign($data) {
    //数据类型检测
    if(!is_array($data)){
        $data = (array)$data;
    }
    ksort($data); //排序
    $code = http_build_query($data); //url编码并生成query字符串
    $sign = sha1($code); //生成签名
    return $sign;
}


/**
 * 二维数组排序
 * @param array $array 排序的数组
 * @param string $key 排序主键
 * @param string $type 排序类型 asc|desc
 * @param bool $reset 是否返回原始主键
 * @return array
 */
function array_order($array, $key, $type = 'asc', $reset = false)
{
    if (empty($array) || !is_array($array)) {
        return $array;
    }
    foreach ($array as $k => $v) {
        $keysvalue[$k] = $v[$key];
    }
    if ($type == 'asc') {
        asort($keysvalue);
    } else {
        arsort($keysvalue);
    }
    $i = 0;
    foreach ($keysvalue as $k => $v) {
        $i++;
        if ($reset) {
            $new_array[$k] = $array[$k];
        } else {
            $new_array[$i] = $array[$k];
        }
    }
    return $new_array;
}

/**
 * 获取文件或文件大小
 * @param string $directoty 路径
 * @return int
 */
function dir_size($directoty)
{
    $dir_size = 0;
    if ($dir_handle = @opendir($directoty)) {
        while ($filename = readdir($dir_handle)) {
            $subFile = $directoty . DIRECTORY_SEPARATOR . $filename;
            if ($filename == '.' || $filename == '..') {
                continue;
            } elseif (is_dir($subFile)) {
                $dir_size += dir_size($subFile);
            } elseif (is_file($subFile)) {
                $dir_size += filesize($subFile);
            }
        }
        closedir($dir_handle);
    }
    return ($dir_size);
}

/**
 * html代码输入
 */
function html_in($str){
    $str=htmlspecialchars($str);
    if(!get_magic_quotes_gpc()) {
        $str = addslashes($str);
    }
   return $str;
}

/**
 * html代码输出
 */
function html_out($str){
    if(function_exists('htmlspecialchars_decode')){
        $str=htmlspecialchars_decode($str);
    }else{
        $str=html_entity_decode($str);
    }
    $str = stripslashes($str);
    return $str;
}

/**
 * 生成唯一数字
 */
function unique_number()
{
    return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
}

/**
 * 生成随机字符串
 */
function random_str()
{
    $year_code = array('A','B','C','D','E','F','G','H','I','J');
    $order_sn = $year_code[intval(date('Y'))-2010].
    strtoupper(dechex(date('m'))).date('d').
    substr(time(),-5).substr(microtime(),2,5).sprintf('d',rand(0,99));
    return $order_sn;
}

/**
 * 获取唯一值
 */
function user_key() {
	return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

/**
 * 替换函数
 */
function subStrCut($str){
    $len = mb_strlen($str,'utf-8');
    if($len > 3){
        $str = mb_substr($str,0,3,'utf-8');
        $str .= "...";
    }
    return $str;
}
/**
 * 获取根域名
 */
function GetDomainUrl() {
    $domain = $_SERVER["HTTP_HOST"];
    $re_domain = '';
    $domain_postfix_cn_array = array("com", "net", "org", "gov", "edu", "com.cn", "cn");
    $array_domain = explode(".", $domain);
    $array_num = count($array_domain) - 1;
    if ($array_domain[$array_num] == 'cn') {
        if (in_array($array_domain[$array_num - 1], $domain_postfix_cn_array)) {
            $re_domain = $array_domain[$array_num - 2] . "." . $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
        } else {
            $re_domain = $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
        }
    } else {
        $re_domain = $array_domain[$array_num - 1] . "." . $array_domain[$array_num];
    }
    return $re_domain;
}


/**
 * 是否是手机号码
 *
 * @param string $phone	手机号码
 * @return boolean
 */
function is_phone($phone) {
	if (strlen ( $phone ) != 11 || ! preg_match ( '/^1[3|4|5|8][0-9]\d{4,8}$/', $phone )) {
		return false;
	} else {
		return true;
	}
}
/**
 * 验证字符串是否为数字,字母,中文和下划线构成
 * @param string $username
 * @return bool
 */
function is_check_string($str){
	if(preg_match('/^[\x{4e00}-\x{9fa5}\w_]+$/u',$str)){
		return true;
	}else{
		return false;
	}
}
/**
 * 是否为一个合法的email
 * @param sting $email
 * @return boolean
 */
function is_email($email){
	if (filter_var ($email, FILTER_VALIDATE_EMAIL )) {
		return true;
	} else {
		return false;
	}
}
/**
 * 是否为一个合法的url
 * @param string $url
 * @return boolean
 */
function is_url($url){
	if (filter_var ($url, FILTER_VALIDATE_URL )) {
		return true;
	} else {
		return false;
	}
}
/**
 * 是否为一个合法的ip地址
 * @param string $ip
 * @return boolean
 */
function is_ip($ip){
	if (ip2long($ip)) {
		return true;
	} else {
		return false;
	}
}
/**
 * 是否为整数
 * @param int $number
 * @return boolean
 */
function is_number($number){
	if(preg_match('/^[-\+]?\d+$/',$number)){
		return true;
	}else{
		return false;
	}
}
/**
 * 是否为正整数
 * @param int $number
 * @return boolean
 */
function is_positive_number($number){
	if(ctype_digit ($number)){
		return true;
	}else{
		return false;
	}
}
/**
 * 是否为小数
 * @param float $number
 * @return boolean
 */
function is_decimal($number){
	if(preg_match('/^[-\+]?\d+(\.\d+)?$/',$number)){
		return true;
	}else{
		return false;
	}
}
/**
 * 是否为正小数
 * @param float $number
 * @return boolean
 */
function is_positive_decimal($number){
	if(preg_match('/^\d+(\.\d+)?$/',$number)){
		return true;
	}else{
		return false;
	}
}
/**
 * 是否为英文
 * @param string $str
 * @return boolean
 */
function is_english($str){
	if(ctype_alpha($str))
		return true;
	else
		return false;
}
/**
 * 是否为中文
 * @param string $str
 * @return boolean
 */
function is_chinese($str){
	if(preg_match('/^[\x{4e00}-\x{9fa5}]+$/u',$str))
		return true;
	else 
		return false;
}
/**
 * 判断是否为图片
 * @param string $file	图片文件路径
 * @return boolean
 */
function is_image($file){
	if(file_exists($file)&&getimagesize($file===false)){
		return false;
	}else{
		return true;
	}
}
/**
 * 是否为合法的身份证(支持15位和18位)
 * @param string $card
 * @return boolean
 */
function is_card($card){
	if(preg_match('/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$/',$card)||preg_match('/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{4}$/',$card))
		return true;
	else 
		return false;
}
/**
 * 验证日期格式是否正确
 * @param string $date
 * @param string $format
 * @return boolean
 */
function is_date($date,$format='Y-m-d'){
	$t=date_parse_from_format($format,$date);
	if(empty($t['errors'])){
		return true;
	}else{
		return false;
	}
}

// 订单号
function orderSn(){
    return '1'.(intval(date('Y'))-2000).strtoupper(dechex(date('m'))).date('d').substr(time(),-5).substr(microtime(),2,5).sprintf('%d',rand(0,99));
}
