<?php

/**
 * @Author: tekin
 * @Date:   2017-09-17 11:16:31
 * @Last Modified 2017-09-17* @Last Modified time: 2017-09-17 11:16:31
 */
//屏蔽ip
function banip($value1,$value2){
	$ban_range_low=ip2long($value1);
	$ban_range_up=ip2long($value2);
	$ip=ip2long($_SERVER["REMOTE_ADDR"]);			
	if ($ip>=$ban_range_low && $ip<=$ban_range_up)
	{
		echo "对不起,您的IP在被禁止的IP段之中，禁止访问！";
		exit();
	}
}
function get_banip(){
	if(file_exists('./data/banip_config_inc.php')){
		$banip=@file_get_contents('./data/banip_config_inc.php');
		$banip=unserialize($banip);
		return $banip;
	}
	else{
		return false;
	}
}
