<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="vendor/fine-uploader/fine-uploader-gallery.min.css" rel="stylesheet">
	<script src="vendor/fine-uploader/fine-uploader.min.js"></script>		
	<?php require_once("vendor/fine-uploader/templates/gallery.html"); ?>
	<title>Multiple File Upload using FineUploader</title>
	<style>
	body {width:600px;font-family:calibri;}
	</style>
</head>
<body>
    <div id="file-drop-area"></div>
    <script>
        var multiFileUploader = new qq.FineUploader({
            element: document.getElementById("file-drop-area"),
            request: {
                endpoint: 'view/fine-uploader/endpoint.php'
            }
        });
    </script>
</body>
</html>