/*
Navicat MySQL Data Transfer

Source Server         : Local-Mysql5.7.18_3357
Source Server Version : 50718
Source Host           : localhost:3357
Source Database       : mipcms

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2017-10-03 19:20:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mip_access_key
-- ----------------------------
DROP TABLE IF EXISTS `mip_access_key`;
CREATE TABLE `mip_access_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `key` varchar(255) DEFAULT NULL COMMENT 'key字段',
  `type` varchar(255) DEFAULT NULL COMMENT '终端使用',
  PRIMARY KEY (`id`),
  KEY `key` (`key`),
  KEY `type` (`type`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mip_access_key
-- ----------------------------
INSERT INTO `mip_access_key` VALUES ('1', 'PC端', 'qFaLOmUGozsURROtxaAqe87vHSlI0LL1', 'pc');
INSERT INTO `mip_access_key` VALUES ('2', '移动端', 'qFaLOmUGoz9URROtxasqe87vHSlI0LL2', 'wap');
INSERT INTO `mip_access_key` VALUES ('3', 'PC端备用', 'cFaLOmUGoz9URROtxaAqe37vHSlI0LL3', 'pc');
INSERT INTO `mip_access_key` VALUES ('4', 'app', 'qFaLOmUGoz9URR4txaAqe87vHSlI0LL4', 'app');

-- ----------------------------
-- Table structure for mip_roles_access
-- ----------------------------
DROP TABLE IF EXISTS `mip_roles_access`;
CREATE TABLE `mip_roles_access` (
  `group_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `node_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  KEY `groupId` (`group_id`),
  KEY `nodeId` (`node_id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mip_roles_access
-- ----------------------------
INSERT INTO `mip_roles_access` VALUES ('2', '1', '1', '0');
INSERT INTO `mip_roles_access` VALUES ('2', '2', '2', '1');
INSERT INTO `mip_roles_access` VALUES ('2', '3', '3', '2');
INSERT INTO `mip_roles_access` VALUES ('2', '4', '3', '2');
INSERT INTO `mip_roles_access` VALUES ('1', '1', '1', '0');
INSERT INTO `mip_roles_access` VALUES ('1', '2', '2', '1');
INSERT INTO `mip_roles_access` VALUES ('1', '3', '3', '2');
INSERT INTO `mip_roles_access` VALUES ('1', '4', '3', '2');

-- ----------------------------
-- Table structure for mip_roles_node
-- ----------------------------
DROP TABLE IF EXISTS `mip_roles_node`;
CREATE TABLE `mip_roles_node` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '节点类型，1-控制器 | 0-方法',
  `sort` smallint(6) unsigned NOT NULL DEFAULT '50',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `isdelete` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `model_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`),
  KEY `isdelete` (`isdelete`),
  KEY `sort` (`sort`),
  KEY `group_id` (`group_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of mip_roles_node
-- ----------------------------
INSERT INTO `mip_roles_node` VALUES ('1', '0', '0', 'ask', '问答模块', '', '1', '1', '50', '1', '0', null);
INSERT INTO `mip_roles_node` VALUES ('2', '1', '0', 'answer', '回答', '', '2', '1', '50', '1', '0', null);
INSERT INTO `mip_roles_node` VALUES ('3', '2', '0', 'answerDel', '回答删除', '', '3', '1', '50', '1', '0', null);
INSERT INTO `mip_roles_node` VALUES ('4', '2', '0', 'answerEdit', '回答编辑', '', '3', '1', '50', '1', '0', null);
