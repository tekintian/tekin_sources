<?php
//定义get 过滤字符
$getfilter = "'|(and|or)\\b.+?(>|<|=|in|like)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
//定义post 过滤字符
$postfilter = "\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";
//定义cookie 过滤字符
$cookiefilter = "\\b(and|or)\\b.{1,6}?(=|>|<|\\bin\\b|\\blike\\b)|\\/\\*.+?\\*\\/|<\\s*script\\b|\\bEXEC\\b|UNION.+?SELECT|UPDATE.+?SET|INSERT\\s+INTO.+?VALUES|(SELECT|DELETE).+?FROM|(CREATE|ALTER|DROP|TRUNCATE)\\s+(TABLE|DATABASE)";

/**
 *	执行过滤函数
 * Stops an attack.
 * @author     (Tekin <tekintian@gmail.com>)
 * @param      <type>  $StrFiltKey    The string filt key
 * @param      <type>  $StrFiltValue  The string filt value
 * @param      string  $ArrFiltReq    The arr filt request
 */
function StopAttack($StrFiltKey, $StrFiltValue, $ArrFiltReq){
	if (is_array($StrFiltValue)) {
		$StrFiltValue = implode($StrFiltValue);
	}
	if (preg_match("/" . $ArrFiltReq . "/is", $StrFiltValue) == 1) {
		print "非法操作!";exit();
	}
}
//循环执行$_GET变量过滤
foreach ($_GET as $key => $value) {
	StopAttack($key, $value, $getfilter);
}
//循环执行$_POST变量过滤
foreach ($_POST as $key => $value) {
	StopAttack($key, $value, $postfilter);
}
//循环执行$_COOKIE变量过滤
foreach ($_COOKIE as $key => $value) {
	StopAttack($key, $value, $cookiefilter);
}
